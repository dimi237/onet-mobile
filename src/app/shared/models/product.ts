export interface Product {
  name: string;
  brand: string;
  ratings: {rating: number; reviews: string };
  prices: {price: number; discount: number};
  features:{
    display: string;
    memory: string;
    camera: string;
    os: string;
    processor: string;
  };
  otherImages: string[];

}
