import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'onet-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  items = [
    {path: '../../assets/images/round-phone_iphone-24px.png', name: 'Smartphones'},
    {path: '../../assets/images/round-headset-24px.png', name: 'Accessories'},
    {path: '../../assets/images/round-tablet_mac-24px.png', name: 'Tablets'},
    {path: '../../assets/images/Group(1).png', name: 'Sale!'},

  ];
  arrivals = [
    {path: '../../assets/images/Group(3).png', name: 'Android phones'},
    {path: '../../assets/images/Group(2).png', name: 'iPhones'}
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
